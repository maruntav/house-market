const express = require('express');
const app = express();
const path = require('path');
const port = process.env.PORT || 3001;
const axios = require('axios');


app.get('/test', (req, res) => {
    axios.get('https://api.nestoria.co.uk/api?encoding=json&pretty=1&action=search_listings&country=uk&listing_type=rent&place_name=ha2&page=2')
        .then(resObj => {
            res.type('json');
            res.send(JSON.stringify(resObj.data));
        })
        .catch(err => console.log('error'));
});

app.get('/nestoria/api/:listing_type/:location/:page_no', (req, res) => {
    let location = req.params.location;
    let listingType = req.params.listing_type;
    let pageNo = req.params.page_no; 
    console.log('page no at backend', pageNo);
    axios.get(`https://api.nestoria.co.uk/api?encoding=json&pretty=1&action=search_listings&country=uk&listing_type=${listingType}&place_name=${location}&page=${pageNo}`)
        .then(respObj => {
            res.type('application/json');
            res.send(JSON.stringify(respObj.data.response));
        })
        .catch(err => console.log(err));
});

app.use(express.static(path.join(__dirname, '../../build')));
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, '../../build', './index.html'));
});

app.listen(port);