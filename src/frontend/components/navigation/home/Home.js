import React from 'react';

const homeLink = (props) => (
    <div className='navigation-home'>
        Home
    </div>
);

export default homeLink;