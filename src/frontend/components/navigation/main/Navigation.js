import React, { Component } from 'react';
import './css/Navigation.css';
import ItemsArray from '../../../utils/navigation/NavigationItem';
import NavigationItems from '../navigation-items/NavigationItems';

export default class Navigation extends Component {

    render() {
        return(
            <div className='navigation'>
                <div className='navigation-emptySpace'></div>
                <NavigationItems items={ItemsArray} />
            </div>
        );
    }
}