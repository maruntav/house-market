import React from 'react';
import { Link } from 'react-router-dom';

const navItems = (props) => (
    <div className='navigation-items'>
        {
            props.items.map(((item) =>
                <Link className='navigation-item' to={item.link} key={`Navigation Item - ${item.name}`}>
                    {item.name}
                </Link>
            ))
        }
    </div>
);

export default navItems;