import React from 'react';
import ListItem from '../list-item/ListItem'

const items = (props) => (
    <div className='listings'>
        {/* {console.log(props.listings)} */}
        {
            props.listings.map((listing, index) => (
                <ListItem listing={listing} index={index} key={`Listing item ` + Math.random()} homeClick={props.homeClick}/>
            ))
        }
    </div>
);

export default items;