import React, { Component } from 'react';
import '../css/HomeInfo.css';

export default class HomeInfo extends Component {
    constructor() {
        super();
        this.state = {};
    }
    componentDidMount() {

    }

    handleTabClick(e) {
        let property = e.target.className.substring(32);
        let ImageAndMap = Array.from(document.querySelector('.home-list-home-info-visuals').children);
        ImageAndMap.slice(1).forEach((item) => item.classList.add('home-list-home-info-visuals-hidden'));
        let showProperty = document.querySelector('.home-list-home-info-' + property);
        showProperty.classList.remove('home-list-home-info-visuals-hidden');
    }

    handleCloseClick(e) {
        let homeInfo = e.target.closest('.homes-list-home-info');
        homeInfo.classList.remove('homes-list-home-info-visible');
        homeInfo.classList.add('homes-list-home-info-hidden');
        let parent = homeInfo.parentElement;
        if(parent.classList.contains('home')) {
            Array.from(parent.children).forEach((child) => {
                child.classList.remove('home-disabled');
            });
        }
        else if(parent.classList.contains('homes-list-page')){
            let homesList = document.querySelector('.homes-list');
            homesList.classList.remove('homes-list-disabled');
        }
    }

    render() {
        // let long = this.props.listing.longitude;
        // let lat = this.props.listing.latitude;
        let long = -0.355013;
        let lat = 51.5818;
        return(
            <div className={'homes-list-home-info homes-list-home-info-hidden'}>
                <div className='home-list-home-info-close' onClick={(e) => this.handleCloseClick(e)}></div>
                <div className='homes-list-home-info-title'>
                    {this.props.listing.title}
                </div>
                <div className='homes-list-home-info-type-price'>
                    <div className='homes-list-home-info-type'>
                        Type: {this.props.listing.property_type}
                    </div>
                    <div className='homes-list-home-info-price'>
                        Price: {this.props.listing.price} {this.props.listing.price_type !== 'fixed' && this.props.listing.price_type}
                    </div>
                </div>
                <div className='home-list-home-info-visuals'>
                    <div className='home-list-home-info-visuals-tabs'>
                        <div className='home-list-home-info-visuals-tab-image' onClick={(e) => this.handleTabClick(e)}>
                            View Image
                        </div>
                        <div className='home-list-home-info-visuals-tab-map' onClick={(e) => this.handleTabClick(e)}>
                            View Map
                        </div>
                    </div>
                    <div className='home-list-home-info-image'>
                        <img src={this.props.listing.img_url} alt='A House' />
                    </div>
                    <div className='home-list-home-info-map home-list-home-info-visuals-hidden'>
                        <iframe src={`https://www.google.com/maps/d/embed?mid=zkqAm4_X6ia8.kj6iIDUiDRdc&hl=en&ll=${lat}, ${long}`} width="400" height="310" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        );
    }
}