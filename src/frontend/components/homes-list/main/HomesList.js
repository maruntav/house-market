import React, { Component } from 'react';
import './css/HomesList.css';  
import FetchListing from '../../../utils/homes-list/fetch/FetchListing';
import ListItems from '../list-items/ListItems'
import PageNumbers from '../page-numbers/PageNumbers';
import HomeInfo from '../home-info/main/js/HomeInfo';

export default class HomesList extends Component{
    constructor(props) {
        super(props);
        this.state = {
            listings: null,
            apiLoaded: false,
            pageNo: 1,
            location: 'Brighton',
            type: this.props.match.path.substring(1),
            currentHome: 0
        };
    }

    componentDidMount() {
        this.fetchData();
    }

    fetchData(pageNo=this.state.pageNo) {
        let state = this.props.location.state || this.state;
        FetchListing(state.type, state.location, this.state.pageNo)
            .then(listings => {
                this.setState({
                    listings: listings,
                    apiLoaded: true
                })
            });
    }

    handlePageNumberClick(e) {
        let pageNo = Number(e.target.textContent);
        this.setState({
            pageNo: pageNo,
            apiLoaded: false
        })
    }

    handleFowardBackClick(e) {
        let direction = e.target.textContent === '>>' ? 1 : -1;
        if(this.state.pageNo + direction > 0 && this.state.pageNo + direction < 6) {
            this.setState((prevState) => ({
                pageNo: prevState.pageNo + direction,
                apiLoaded: false
            }));
        }
    }

    handleHomeClick(e) {
        let index = e.target.closest('.listing-item').getAttribute('home-number');
        this.setState({
            currentHome: Number(index)
        }, () => {
            console.log(this.state.currentHome);
            let homeInfo = document.querySelector('.homes-list-home-info');
            homeInfo.classList.remove('homes-list-home-info-hidden');
            homeInfo.classList.add('homes-list-home-info-visible');
            let homesList = document.querySelector('.homes-list');
            homesList.classList.add('homes-list-disabled');
        });
    }

    componentDidUpdate() {
        if(!this.state.apiLoaded) {
            this.fetchData();
        }
    }

    render() {
        if(!this.state.apiLoaded) {
            return(<div></div>)
        }
        else if(this.state.listings) {
            return(
                <div className='homes-list-page'>
                    <div className='homes-list'>
                        <ListItems listings={this.state.listings} homeClick={(e) => this.handleHomeClick(e)}  />
                        <PageNumbers numberClick={(e) => this.handlePageNumberClick(e)} highlightNum={this.state.pageNo} forBackClick={(e) => this.handleFowardBackClick(e)}/>
                    </div>
                    <HomeInfo listing={this.state.listings[this.state.currentHome]}/>
                </div>
            );
        }
        else {
            return(
                <div className='homes-list-error'>
                    Could not load data, try again?
                </div>
            );
        }  
    }
}