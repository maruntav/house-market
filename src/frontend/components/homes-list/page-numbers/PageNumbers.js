import React from 'react';

const pageNos = (props) => (
    <div className='page-numbers'>
        <div className='page-numbers-direction' onClick={props.forBackClick}>{'<<'}</div>
        {
            [1,2,3,4,5].map(((number, index) => (
                <div className={'page-number' +(props.highlightNum - 1 === index ? ' page-number-highlight' : '')} key={'Page numbers key ' + index} onClick={props.numberClick}>{number}</div>
            )))
        }
        <div className='page-numbers-direction' onClick={props.forBackClick}>{'>>'}</div>
    </div>
);

export default pageNos;