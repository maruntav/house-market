import React from 'react';

const item = (props) => (
    <div className='listing-item' home-number={props.index}>
        <div className='item-title' onClick={props.homeClick}>
            <div className='item-title-text'>{props.listing.title}</div>
        </div>
        <div className='item-description'>
            <div className='summary'>{props.listing.summary}</div>
            <div className='item-image'>
                <img src={props.listing.thumb_url} alt='A house'/>
            </div>
        </div>
    </div>
);

export default item;