import React, { Component } from 'react';
import './css/Home.css';
import Search from '../search/Search';
import HomesNearYou from '../homes-near-you/main/NearYou';
import RemoveMarginChild from '../../../utils/home/near-you/RemoveMargins';
// import ChangeBtnStatus from '../../../utils/home/button-status/ButtonClassMan';
import FetchListing from '../../../utils/homes-list/fetch/FetchListing';
import HomeInfo from '../../homes-list/home-info/main/js/HomeInfo';

export default class Home extends Component {
    constructor() {
        super();
        this.state = {
            searchField: '',
            listings: null,
            apiLoaded: false,
            currentIndex: 0
        }
    }

    componentDidMount() {
        FetchListing('buy', 'Brighton', '1')
            .then((listings) => {
                console.log('got something');
                console.log(listings);
                this.setState({
                    listings: listings,
                    apiLoaded: true
                })
            });
        if(this.state.apiLoaded && this.state.listing) {
            RemoveMarginChild('images');
        }
        // ChangeBtnStatus();
    }

    handleSeachInputChange(e) {
        this.setState({
            searchField: e.target.value
        });
    }

    handleBuyClick(e, type) {
        this.props.history.push('/homes-list', {
            type: type,
            location: this.state.searchField,
            pageNo: 1
        });
    }

    homesNearClick(e) {
        let targetImage = e.target.closest('.image');
        let images = targetImage.parentElement;
        let index = Array.from(images.children).findIndex((image) => image === targetImage);  
        this.setState({
            currentIndex: index
        });
        let homeInfo = document.querySelector('.home .homes-list-home-info');
        console.log(homeInfo);
        homeInfo.classList.remove('homes-list-home-info-hidden');
        homeInfo.classList.add('homes-list-home-info-visible');

        let home = document.querySelector('.home');
        let children = Array.from(home.children);
        let childrenToDisable = children
            .filter((child) => !child.classList.contains('homes-list-home-info'));
        childrenToDisable.forEach((child) => {
            child.classList.add('home-disabled');
        });
    }


    render() {
        if(!this.state.apiLoaded) {
            return <div>Loading</div>
        }
        else if(this.state.apiLoaded && !this.state.listings) {
            return <div>Cannot load data</div>
        }
        return(
            <div className='home'>
                <Search searchInputVal={this.state.searchField} searchChange={(e) => this.handleSeachInputChange(e)} 
                    buyClick={(e) => this.handleBuyClick(e, 'buy')}
                    rentClick={(e) => this.handleBuyClick(e, 'rent')}
                />
                <HomesNearYou listings={this.state.listings} imageClick={(e) => this.homesNearClick(e)} />
                <HomeInfo listing={this.state.listings[this.state.currentIndex]} />
                <div className='home-maps'>
                </div>
            </div>
        );
    }
}