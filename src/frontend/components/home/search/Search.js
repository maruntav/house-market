import React from 'react';
import Image from '../../../utils/home/house.jpg';
import SearchBox from './search-box/SearchBox'

const search = (props) => (
    <div className='home-search'>
        <div className='home-search-image'>
            <img src={Image} alt='A House' />
            <SearchBox searchInputVal={props.searchInputVal} searchChange={props.searchChange} buyClick={props.buyClick} rentClick={props.rentClick}/>
        </div>
    </div>
);

export default search;