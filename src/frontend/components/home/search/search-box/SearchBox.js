import React from 'react';

const searchBox = (props) => (
    <div className='home-search-searchBox'>
        <div className='header'>Find your happy</div>
        <div className='information'>Search for properties</div>
        <div className='search-area'>
            <input type='text' className='search-input' value={props.searchInputVal} onChange={props.searchChange}/>
            <div  className='buy-button' onClick={props.buyClick}>Buy</div>
            <div className='rent-button' onClick={props.rentClick}>Rent</div>
        </div>
              
    </div>
);

export default searchBox;