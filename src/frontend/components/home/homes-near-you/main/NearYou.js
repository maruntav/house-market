import React from 'react';
import images from './images/Images';
import LeftRightButton from '../left-right/Button';
import FetchListing from '../../../../utils/homes-list/fetch/FetchListing';
const nearYou = (props) => {
    return(
        <div className='home-nearYou'>
            <div className='header'>Buy</div>
            <div className='images-buttons'>
                <LeftRightButton direction={'left'} />
                <div className='images'>
                    {
                        props.listings.map((listing, index) => (
                            <div className='image' key={listing.title + ' ' + index} onClick={props.imageClick}>
                                <img src={listing.img_url} alt={listing.title + index} />
                                <div className='days-since'>{listing.updated_in_days_formatted}</div>
                                <div className='bottom-information'>
                                    <div className='status'>{listing.price}</div>
                                    <div className='information'>{listing.title}</div>
                                </div>
                            </div>
                        ))
                    }
                </div>
                <LeftRightButton direction={'right'} />
            </div>
            
        </div>
    );
};

export default nearYou;