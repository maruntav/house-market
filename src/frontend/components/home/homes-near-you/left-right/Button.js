import React from 'react';
import HandleScroll from '../../../../utils/home/near-you-scroll/HandleScrollClick';
import changeButtonColour from '../../../../utils/home/button-status/ButtonClassMan';

const button = (props) => (
    <div className='button-wrapper' onClick={(e) => {HandleScroll(props.direction); changeButtonColour(props.direction)}}>
        <div className={props.direction}>
            <div className='arrow-wrapper'>
                <div className={'arrow' + (props.direction === 'left' ? ' arrow-disabled' : '')}></div>
            </div>
        </div>
    </div>
);

export default button;