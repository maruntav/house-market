import React, { Component } from 'react';
import './App.css';
import { Route } from 'react-router-dom';
import Navigation from './components/navigation/main/Navigation';
import Home from './components/home/main/Home';
import HomesList from './components/homes-list/main/HomesList';

class App extends Component {

  render() {
    return (
      <div className="App">
        <Navigation />
        <Route path='/homes-list' component={HomesList} />
        <Route path='/buy' component={HomesList} />
        <Route path='/rent' component={HomesList} />
        <Route exact path='/' component={Home} />
        <a href="https://www.nestoria.co.uk"><img src="https://resources.nestimg.com/nestoria/img/pbr_v1.png" alt="powered by nestoria.co.uk" width="200" height="22" /></a>
		  </div>
    );
  }
}

export default App;