const items = [
    {
        name: 'Home',
        link: '/'
    },
    {
        name: 'Buy',
        link: '/buy'
    },
    {
        name: 'Rent',
        link: '/rent'
    },
]

export default items;