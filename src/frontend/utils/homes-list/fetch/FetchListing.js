
export default function getListing(listingType, location, pageNo) {
    return fetch(`nestoria/api/${listingType}/${location}/${pageNo}`)
    .then((res) => {
        return res.json();
    })
    .then((jsonObj) => {
        return jsonObj.listings;
    })
    .then(listings => {
        console.log(listings);
        return listings.map((listing) => ({
            listing_type: listing.listing_type,
            title: listing.title,
            thumb_url: listing.thumb_url,
            price: listing.price_formatted,
            summary: listing.summary,
            updated_in_days_formatted: listing.updated_in_days_formatted,
            lister_url: listing.lister_url,
            price_type: listing.price_type,
            longitude: listing.longitude,
            latitude: listing.latitude,
            property_type: listing.property_type,
            img_url: listing.img_url,
        }));
    });
}