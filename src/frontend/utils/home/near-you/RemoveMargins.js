const addClassForFirstAndLastChild = (className) => {
    let images = document.querySelector('.images');
    let firstChild = images.firstElementChild;
    let lastChild = images.lastElementChild;
    firstChild.classList.add('image-first-child');
    lastChild.classList.add('image-last-child');
};

export default addClassForFirstAndLastChild;