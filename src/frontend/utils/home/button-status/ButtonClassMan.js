export default function changeButtonColour(direction) {
    direction === 'left' ? leftScroll() : rightScroll();
}

function leftScroll() {
    let imageWrapper = document.querySelector('.images');
    let left, right;
    [left, right] = getLeftRight();
    clearClassName(left, right);
    if(imageWrapper.scrollLeft <= 352) {
        left.className += ' arrow-disabled';
    }
}

function rightScroll() {
    let imageWrapper = document.querySelector('.images');
    // console.log(imageWrapper.scrollLeft + Math.ceil(imageWrapper.clientWidth ) + 352);
    let left, right;
    [left, right] = getLeftRight();
    clearClassName(left, right);
    if(imageWrapper.scrollLeft + Math.ceil(imageWrapper.clientWidth ) + 352 >= imageWrapper.scrollWidth) {
        right.className += ' arrow-disabled';
    }
}

function getLeftRight() {
    let buttons = document.querySelectorAll('.home-nearYou .images-buttons .button-wrapper');
    let left = buttons[0].firstElementChild.firstElementChild.firstElementChild;
    let right = buttons[1].firstElementChild.firstElementChild.firstElementChild;
    return [left, right];
}

function clearClassName(left, right) {
    left.className = 'arrow';
    right.className = 'arrow';
}