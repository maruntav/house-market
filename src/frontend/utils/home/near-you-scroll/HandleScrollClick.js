export default function(direction) {
    let images = document.getElementsByClassName('images')[0];
    let fullWidth = 320 + 16 + 16;
    if(direction === 'left') {
        images.scrollLeft -= fullWidth;
    }
    else {
        images.scrollLeft += fullWidth;
    }
}